from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'core.views.homepage', name='homepage'),
    url(r'^supplier/', include('supplier.urls', namespace='supplier')),
    url(r'^admin/', include(admin.site.urls)),
)
