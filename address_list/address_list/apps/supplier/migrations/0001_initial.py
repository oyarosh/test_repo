# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Addresses',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('address_line_1', models.CharField(default=b'', max_length=255, null=True, verbose_name='Address line 1', blank=True)),
                ('address_line_2', models.CharField(default=b'', max_length=255, null=True, verbose_name='Address line 2', blank=True)),
                ('phone', models.CharField(default=b'', max_length=255, null=True, verbose_name='Phone', blank=True)),
                ('town', models.CharField(default=b'', max_length=255, null=True, verbose_name='Town', blank=True)),
                ('post_code', models.PositiveIntegerField(null=True)),
                ('fax', models.CharField(default=b'', max_length=255, null=True, verbose_name='Fax', blank=True)),
                ('email', models.EmailField(unique=True, max_length=75)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Suppliers',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('supplier_name', models.CharField(max_length=150, blank=True)),
                ('addresses', models.ManyToManyField(to='supplier.Addresses', verbose_name='Suppliers addresses', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
