from django.conf.urls import patterns, url

from supplier.views import SupplierList, SupplierCreate, \
    SupplierUpdate, SupplierDelete, SupplierCreateAddress, \
    SupplierSearch

urlpatterns = patterns(
    '',
    url(r'^list/$', SupplierList.as_view(), name='list'),
    url(r'^create/$', SupplierCreate.as_view(), name='create'),
    url(r'^create_address/$', SupplierCreateAddress.as_view(), name='create_address'),
    url(r'^update/(?P<pk>\d+)/$', SupplierUpdate.as_view(), name='update'),
    url(r'^delete/(?P<pk>\d+)/$', SupplierDelete.as_view(), name='delete'),
    url(r'^search/$', SupplierSearch.as_view(), name='search'),
)
