from django import forms
from supplier.models import Suppliers


class SupplierForm(forms.ModelForm):

    class Meta:
        model = Suppliers
        fields = '__all__'
