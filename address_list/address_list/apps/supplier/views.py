from django.views.generic import ListView, UpdateView, CreateView, DeleteView

from django.core.urlresolvers import reverse_lazy

from supplier.models import Suppliers, Addresses
from supplier.forms import SupplierForm


class SupplierMixin(object):

    def get_queryset(self):
        return Suppliers.objects.all()


class SupplierList(SupplierMixin, ListView):
    template_name = 'supplier/list.html'


class SupplierCreate(SupplierMixin, CreateView):
    form_class = SupplierForm
    success_url = reverse_lazy('supplier:list')
    template_name = 'supplier/create.html'


class SupplierUpdate(SupplierMixin, UpdateView):
    model = Suppliers
    success_url = reverse_lazy('supplier:list')
    template_name = 'supplier/update.html'


class SupplierDelete(SupplierMixin, DeleteView):
    model = Suppliers
    success_url = reverse_lazy('supplier:list')


class SupplierSearch(ListView):
    template_name = 'supplier/filtered_result.html'

    def get_queryset(self):
        queryset = Suppliers.objects.all()
        return queryset.filter(
            supplier_name__icontains=self.request.GET['query'])


class AddressMixin(object):

    def get_queryset(self):
        return Addresses.objects.all()


class SupplierCreateAddress(AddressMixin, CreateView):
    model = Addresses
    success_url = reverse_lazy('supplier:create')
    template_name = 'supplier/create_addresses.html'
