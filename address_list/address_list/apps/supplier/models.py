from django.db import models


class Suppliers(models.Model):
    supplier_name = models.CharField(max_length=150, blank=True)
    addresses = models.ManyToManyField('Addresses', blank=True,
                                       verbose_name=u'Suppliers addresses')


class Addresses(models.Model):
    address_line_1 = models.CharField(verbose_name=u'Address line 1',
                                      max_length=255, default='',
                                      null=True, blank=True)
    address_line_2 = models.CharField(verbose_name=u'Address line 2',
                                      max_length=255, default='',
                                      null=True, blank=True)
    phone = models.CharField(u'Phone',
                             max_length=255, default='',
                             null=True, blank=True)
    town = models.CharField(u'Town',
                            max_length=255, default='',
                            null=True, blank=True)
    post_code = models.PositiveIntegerField(null=True)
    fax = models.CharField(u'Fax',
                           max_length=255, default='',
                           null=True, blank=True)
    email = models.EmailField(unique=True)
